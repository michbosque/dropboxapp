//
//  UserContentViewController.m
//  DropboxApp
//
//  Created by Bosque, Michelle L. S. on 23/02/2015.
//  Copyright (c) 2015 Accenture. All rights reserved.
//

#import "UserContentViewController.h"
#import "Content.h"

#define ROWS @"rows"
#define TITLE @"title"
static const int TABLE_OFFSET = 0;
static NSString *FACTS_URL = @"https://dl.dropboxusercontent.com/u/746330/facts.json";

@interface UserContentViewController ()

@end

@implementation UserContentViewController
@synthesize userTableView;
@synthesize userArray;
@synthesize refreshControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self loadInterface];
    [self getDropboxUserContent];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"Cell";

    UserCustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UserCustomTableViewCell alloc] init];
    }
    Content *content = [userArray objectAtIndex:indexPath.row];
    cell.titleLabel.text = content.title;
    cell.descriptionLabel.text = content.description;
    
    cell.imgView.image = [UIImage imageNamed:@"placeholder.jpg"];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:content.imageHref]];
        UIImage *image = [UIImage imageWithData:data];
        dispatch_async(dispatch_get_main_queue(), ^{
            if(image)
                cell.imgView.image = image;
        });
    });
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [userArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}


#pragma mark Private methods
- (void)loadInterface{
    userTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, TABLE_OFFSET, self.view.frame.size.width, self.view.frame.size.height)];
    userTableView.delegate = self;
    userTableView.dataSource = self;
    [self.view addSubview:userTableView];
    
    userArray = [[NSMutableArray alloc] init];
    [userArray retain];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [userTableView addSubview:refreshControl];
    
    
    // Create prototype cell for calculating height
    static NSString *CellIdentifier = @"Cell";
    [self.searchDisplayController.searchResultsTableView registerClass:[UserCustomTableViewCell class] forCellReuseIdentifier:CellIdentifier];
    self.cellPrototype = [self.searchDisplayController.searchResultsTableView dequeueReusableCellWithIdentifier:CellIdentifier];
}

- (void) handleRefresh:(UIRefreshControl *)control {
    [self getDropboxUserContent];
}

- (void)getDropboxUserContent {
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:FACTS_URL]];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if(!error){
                                   NSString * responseString = [[[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSASCIIStringEncoding] autorelease];
                                   NSData * responseData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                                   NSError * error;
                                   NSDictionary * responseDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
                                   NSArray *rows = [responseDictionary objectForKey:ROWS];
                                   
                                   if([responseDictionary objectForKey:TITLE]){
                                       self.title = [responseDictionary objectForKey:TITLE];
                                   }
                                   
                                   if(rows){
                                       for(id row in rows){
                                           Content *content = [[Content alloc] initWithDictionary:(NSDictionary *)row];
                                           [userArray addObject:content];
                                           
                                       }
                                       [self filterNonEmptyValues];
                                       [userTableView reloadData];
                                       [refreshControl endRefreshing];
                                   }
                                   
                               }
    }];
}

- (void)filterNonEmptyValues {
    NSMutableArray *filteredArray = [[NSMutableArray alloc] init];
    for(Content *content in userArray){
        if(![content.title isEqualToString:@""] && ![content.description isEqualToString:@""] && ![content.imageHref isEqualToString:@""]){
            [filteredArray addObject:content];
        }
    }
    userArray = filteredArray;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
