//
//  UserContentViewController.h
//  DropboxApp
//
//  Created by Bosque, Michelle L. S. on 23/02/2015.
//  Copyright (c) 2015 Accenture. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserCustomTableViewCell.h"

@interface UserContentViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *userTableView;
@property (nonatomic, strong) NSMutableArray *userArray;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (strong) UserCustomTableViewCell *cellPrototype;

@end
