//
//  Content.m
//  DropboxApp
//
//  Created by Bosque, Michelle L. S. on 23/02/2015.
//  Copyright (c) 2015 Accenture. All rights reserved.
//

#import "Content.h"

#define TITLE @"title"
#define DESCRIPTION @"description"
#define IMAGEHREF @"imageHref"

@implementation Content
@synthesize title;
@synthesize description;
@synthesize imageHref;

- (id) initWithDictionary:(NSDictionary*)inputValues {
    self = [super init];
    
    title = @"";
    if([inputValues objectForKey:TITLE] && [inputValues objectForKey:TITLE] != [NSNull null]){
        title = [inputValues objectForKey:TITLE];
    }
    [title retain];
    
    description = @"";
    if([inputValues objectForKey:DESCRIPTION] && [inputValues objectForKey:DESCRIPTION] != [NSNull null]){
        description = [inputValues objectForKey:DESCRIPTION];
    }
    [description retain];
    
    imageHref = @"";
    if([inputValues objectForKey:IMAGEHREF] && [inputValues objectForKey:IMAGEHREF] != [NSNull null]){
        imageHref = [inputValues objectForKey:IMAGEHREF];
    }
    [imageHref retain];
    
    return self;
}

@end
