//
//  Content.h
//  DropboxApp
//
//  Created by Bosque, Michelle L. S. on 23/02/2015.
//  Copyright (c) 2015 Accenture. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Content : NSObject

@property (strong, nonatomic) IBOutlet NSString *title;
@property (strong, nonatomic) IBOutlet NSString *description;
@property (strong, nonatomic) IBOutlet NSString *imageHref;

- (id) initWithDictionary:(NSDictionary*)inputValues;

@end
